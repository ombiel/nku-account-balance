let React = window.React = require('react');
let reactRender = require('-aek/react/utils/react-render');
let { VBox, CBox } = require('-components/layout');
let Container = require('-components/container');
let {Message} = require("@ombiel/aek-lib/react/components/message");
let {BasicSegment} = require("@ombiel/aek-lib/react/components/segment");
let Header = require("@ombiel/aek-lib/react/components/header");
let {Listview,Item} = require("@ombiel/aek-lib/react/components/listview");
let Panel = require("@ombiel/aek-lib/react/components/panel");
let Page = require('-components/page');
let Button = require("@ombiel/aek-lib/react/components/button");

const GenericStore = require('./scripts/generic-store');
const Config = require('./modules/config');

let transactionsToShow = Config.transactionsToShow;

let Screen = React.createClass({

  getInitialState:function(){
    return ({showMoreLoading:false});
  },

  componentDidMount:function() {
    GenericStore.fetchAccount();
    // this is required to trigger a rerender on SimpleStore update
    // only works thanks to React's fast virtual DOM; would impact performance otherwise
    GenericStore.on('change', () => {
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    // we also have a parallel implementation in GenericUtils if you want to avoid this
    if(!String.prototype.endsWith) {
      String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }
  },

  componentWillUnmount:function() {
    GenericStore.off('change');
  },

  showMore:function() {
    this.setState({showMoreLoading:true});
    transactionsToShow = transactionsToShow + Config.transactionsToShow;
    this.setState({showMoreLoading:false});
  },

  render:function() {
    let balance = null;
    let activity = null;
    let activityList = null;
    let activityListToShow = null;
    let seeMoreButton = null;
    let loading = GenericStore.get('account.$$$Loading');

    if (GenericStore.get('account.accountData')){
      balance = GenericStore.get('account.accountData').DATA.BALANCE;
      activity = GenericStore.get('account.accountData').DATA.ACTVITY;

      activityList = activity.map((act, index) => {
        return (
          <Listview uniformLabels key={'transaction-' + index}>
            <Item label={Config.language.dateText}>{act.DATE}</Item>
            <Item label={Config.language.amountText}>{Config.currency_prefix}{act.AMOUNT}</Item>
            <Item label={Config.language.descriptionText}>{act.DESCRIPTION}</Item>
          </Listview>
        );
      });

      activityListToShow = activityList.slice(0,transactionsToShow);

      seeMoreButton = transactionsToShow < activityList.length ?
        <Button size='tiny' loading={this.state.showMoreLoading} compact onClick={this.showMore}>{Config.language.showMoreText}</Button>
        : null;
      loading = false;
    }

    return (
      <Page>
        <VBox>
          <BasicSegment data-flex={0} key='available-balance'>
            <Message compact loading={loading}><Header level='2' textAlign='center'>{Config.language.availableBalanceText}{Config.currency_prefix}{balance}</Header></Message>
          </BasicSegment>
          <Panel>
            <BasicSegment loading={loading}>
              {activityListToShow}
              {seeMoreButton}
              </BasicSegment>
            </Panel>
            <div data-flex={0} key='footer'>
              <BasicSegment>
                <Button loading={loading} variation='prime' fluid href={Config.payNowUrl}>{Config.language.payNowText}</Button>
              </BasicSegment>
            </div>
          </VBox>
        </Page>
      );
    }
  });

  reactRender(<Screen />);
