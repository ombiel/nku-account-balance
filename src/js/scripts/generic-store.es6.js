const SimpleStore = require('-aek/simple-store');
const request = require('-aek/request');
const moment = require('moment');
const _ = require('-aek/utils');
const Notifier = require('-aek/notifier');

const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'aek2-test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;

class GenericStore extends SimpleStore {
  constructor() {
    super({
      initialState:{
        account:{
          accountData: null,
          accountError: null,
          $$$Loading: true
        }
      },
      plugins:[
        loggerPlugin(),
        localStoragePlugin(storageKey)
      ]
    });

    // you can modify state values here if you absolutely have to, i.e. if you're checking for something on startup
  }

  dispatchAccount(ctx, results, isError, errorMessage, loading) {
    // moment doesn't serialise well, so store the formatted string to re-use later
    ctx.dispatch({
      name: 'account',
      error: isError,
      extend:{
        accountData: results,
        accountError: errorMessage,
        $$$viewLoading: loading
      }
    });
  }

  // an example of a default parameter value - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
  fetchAccount() {
    let ctx = this.context({ group:'ACCOUNT', path:'account' });

    // get any existing data from the store for if the service call fails
    let existingData = this.state.account.accountData;
    this.dispatchAccount(ctx, existingData, false, null, true);

    request.action('get-account').end((err, res) => {
      if(!err && res && res.body !== null && res.text.indexOf('{}') !== 0) {
          this.dispatchAccount(ctx, res.body, false, null, false);
      } else {
        let error = _.has(res, 'body') ? res.body : 'Unknown response from server.';
        this.dispatchAccount(ctx, existingData, true, error, false);
        this.genericNotifier('Server Failure', error, true, 'error');
      }
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // more examples of default parameters values
  genericNotifier(title, message, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

}

module.exports = new GenericStore();
